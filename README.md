# zettelkasten reader

This is a simple reader of the markdown file (.md) exported from the Java software Zettelkasten - nach Luhmann (take a look at http://zettelkasten.danielluedecke.de/) and that runs in Linux terminal and have some little functionalities.
To get this file, go to Zettelkasten software: File/export.
My intent with this script was to be able to easily see my zettels on my cellphone through [Termux app](https://termux.com/). This script was made in shell script.

It is not  neccessary to install, but probably it will be necessary to give permition to run the script. Type `chmod +x zett` to do so.
Just run run it `. zett.sh [OPTION]` or `shell .zett [OPTION]`

zett [OPTION]
    
* -h or --help    display this help

* EMPTY           If no argument is given, all the titles will be displayed

* NUMBER          print the content of the zettel by the number of entry specified. For exemple `. zett 55` will print the zettel number 55

* TERM            Search in Zettel titles a certain term. For exemple `. zett apples` will display all titles that contain that word. It only search one word.